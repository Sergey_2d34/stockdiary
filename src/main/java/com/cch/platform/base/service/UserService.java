package com.cch.platform.base.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Service;

import com.cch.platform.base.bean.BaseUser;
import com.cch.platform.dao.HibernateDao;
import com.cch.platform.dao.PageObject;
import com.cch.platform.service.BaseService;
import com.cch.platform.util.StringUtil;

@Service
public class UserService extends BaseService<BaseUser>{
	
	@SuppressWarnings("unchecked")
	public BaseUser checkUser(String userName){
		String hql="from BaseUser where userName=?";
		List<BaseUser> lu=beanDao.find(hql, userName);
		if(lu.size()>0){
			return lu.get(0);
		}
		return null;
	}

}
