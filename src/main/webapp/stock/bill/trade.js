var edittype="";

$(function(){
	$('#dlg').dialog("close");
	$("#dg").edatagrid({
		onDblClickRow : function(index, rowData){
			edittype="beginedit";
			$('#ff').form("clear");
			$('#ff').form("load",rowData);
			$('#dlg').dialog("open");
			edittype="edit";
		}
	});
	
	var opts=$("#dg").datagrid("getColumnOption","stockName");
	opts.formatter=function (value,row,index){
		return plat.FlexUtil.getName("SD_STOCK_SEL",row.stockCode);
	}
});

function click_add(){
	edittype="add";
	$('#ff').form("clear");
	$('#dlg').dialog("open");
}

function click_delete(){
	$("#dg").edatagrid("destroyRow");
}

function dlg_click_save(){
	$('#ff').form('ajaxSubmit',{ 
			url: plat.fullUrl("/bill/stocktrade/save.do"),
			success: function(data){
				if(data.sucesse==true){
					dlg_click_cancle();
					$('#dg').edatagrid("reload");
				}
			} 
		});
}

function dlg_click_delete(){
	if(edittype!="edit"){
		dlg_click_cancle();
		return;
	}
	$('#ff').form('ajaxSubmit',{ 
		url: plat.fullUrl("/bill/stocktrade/delete.do"),
		success:function(data){
			if(data.sucesse==true){
				dlg_click_cancle();
				$('#dg').edatagrid("reload");
			}
		}
	});
}

function dlg_click_cancle(){
	edittype="";
	$('#dlg').dialog("close");
}

function dlg_auto_calfee(){
	if(edittype!='add'&&edittype!='edit')
		return;
	
	var data=$('#ff').form('getData');
	if(data.billCatory&&data.stockCode&&data.stockCnt&&data.stockPrice){
		data.stockAllpric=data.stockCnt*data.stockPrice;
		$.post(plat.fullUrl("/stock/tradefee/getFee.do"), 
				data,
				function(data){
					$('#ff').form("load", data);
				}
			);
	}
}
