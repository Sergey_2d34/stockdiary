/**
 * 
 */
function doInit() {
	refreshMenu();
}

// 加载菜单
function refreshMenu(){
	$.post(plat.fullUrl("/base/menucontroller/query.do"), function(data) {
		if(data.menus){
			$('#index_menu').tree("loadData",data.menus);
		}
	});
}

function changeMenuParent(node){
	var pnode=$('#index_menu').tree("getParent",node.target);
	var data=node.attributes;
	var pcode="";
	if(pnode!=null)pcode=pnode.attributes.code;
	if(data.pcode==pcode){
		return;
	}
	data.pcode=pcode;
	$.post(plat.fullUrl("/base/menucontroller/save.do"), 
		data,
		function(data){
			refreshMenu();
	});
}

function click_menu(node){
	var code=node.attributes.code;
	$.post(plat.fullUrl("/base/menucontroller/getbycode.do"), 
		{code:code},
		function(data) {
			if(data.menu){
				$("#ff").form("load",data.menu);
			}
		}
	);
}

function click_addTop(){
	$("#ff").form("clear");
}

function click_add(){
	var node=$('#index_menu').tree("getSelected");
	if(node==null){
		$.messager.alert('错误','请选择上级菜单!','error');
		return;
	}
	var menu=$("#ff").form("getData");
	menu.pcode=menu.code;
	menu.code=menu.code+"_";
	$("#ff").form("load",menu);
}

function click_save(){
	$('#ff').form('ajaxSubmit',{ 
			url: plat.fullUrl("/base/menucontroller/save.do"),
			success: function(data){
				refreshMenu();
			}
		});
}

function click_delete(){
	$('#ff').form('ajaxSubmit',{ 
			url: plat.fullUrl("/base/menucontroller/delete.do"),
			success: function(data){
				refreshMenu();
			}
		});
}

